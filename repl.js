const repl = require("repl");
const { dbLoader } = require("./dist/loaders/db");
var glob = require("glob"),
  path = require("path");

// Get all models
let models = [];
glob.sync("./dist/models/**/*.js").forEach(function(file) {
  const model = require(path.resolve(file));
  models.push(model);
});

// Start the REPL
async function bootstrap() {
  const replServer = repl.start({
    prompt: "Node Console > "
  });
  const connection = await dbLoader();
  replServer.context.connection = connection;
  // Map model's repos to context
  models.forEach(model => {
    for (let key of Object.keys(model)) {
      replServer.context[key] = connection.getRepository(key);
    }
  });
}

bootstrap();
