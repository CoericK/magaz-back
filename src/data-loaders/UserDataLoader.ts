import Container from "typedi";
import DataLoader from "dataloader";
import { UserDAL } from "../components/users/UserDAL";
import { User } from "../models/User";

export const userLoader = (): DataLoader<string, User> => {
  return new DataLoader(async keys => {
    const userIds = keys as string[];

    const users = await Container.get(UserDAL).findByIds(userIds);

    const usersMap: Record<string, User> = {};
    users.forEach(user => {
      usersMap[user.id] = user;
    });

    return userIds.map(key => usersMap[key]);
  });
};
