import { userLoader } from "../../../data-loaders/UserDataLoader";
import { productLoader } from "../../../data-loaders/ProductDataLoader";
import { cartLoader } from "../../../data-loaders/OrderDataLoader";
import DataLoader from "dataloader";
import { User } from "../../../models/User";
import { Product } from "../../../models/Product";
import { Order } from "../../../models/Order";

export function initDataLoaders(): Loaders {
  return {
    userLoader: userLoader(),
    productLoader: productLoader(),
    cartLoader: cartLoader()
  };
}

export interface Loaders {
  userLoader: DataLoader<string, User>;
  productLoader: DataLoader<string, Product>;
  cartLoader: DataLoader<string, Order>;
}
