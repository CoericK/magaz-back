import * as TypeGraphQL from "type-graphql";
import { GraphQLSchema } from "graphql";
import { UserResolver } from "../../components/users/UserResolver";
import { OrderResolver } from "../../components/orders/OrderResolver";
import { ProductResolver } from "../../components/products/ProductResolver";
import { OrderItemResolver } from "../../components/orders/OrderItemResolver";
import Container from "typedi";
import { authChecker } from "../helpers/authChecker";
import { env } from "../../config/env";
import { RefreshTokenResolver } from "../../components/refresh-tokens/RefreshTokenResolver";
import { SignUpResolver } from "../../components/auth/sign-up/SignUpResolver";
import { SignInResolver } from "../../components/auth/sign-in/SignInResolver";
import { UserConfirmResolver } from "../../components/auth/user-confirm/UserConfirmResolver";

export const schemaLoader = async (): Promise<GraphQLSchema> => {
  return await TypeGraphQL.buildSchema({
    resolvers: [
      UserResolver,
      OrderResolver,
      RefreshTokenResolver,
      SignUpResolver,
      SignInResolver,
      UserConfirmResolver,
      ProductResolver,
      OrderItemResolver
    ],
    container: Container,
    validate: true,
    authChecker: authChecker,
    emitSchemaFile: env.emitSchema && {
      path: "./schema.gql",
      commentDescriptions: true
    }
  });
};
