import express from "express";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import { env } from "../../config/env";

const app = express();

app.use(cookieParser());

app.use(helmet());

if (env.logHTTP) {
  app.use(morgan("tiny"));
}

export default app;
