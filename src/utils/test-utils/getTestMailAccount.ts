import nodemailer, { TestAccount } from "nodemailer";

let testMailAccount: TestAccount;

export const getTestMailAccount = async (): Promise<TestAccount> => {
  testMailAccount = await nodemailer.createTestAccount();

  return testMailAccount;
};
