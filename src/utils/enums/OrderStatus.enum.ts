export enum OrderStatusEnum {
  "draft" = 0,
  "placed" = 1,
  "in_process" = 2,
  "in_delivery" = 3,
  "completed" = 4,
  "canceled" = 5,
  "in_refund" = 6
}
