/*
 * GQL-specific files
 */
import { ObjectType, Field } from "type-graphql";
import DataLoader from "dataloader";
import { Request } from "express";
import { User } from "../../models/User";
import { Product } from "../../models/Product";
import { Order } from "../../models/Order";
import { Loaders } from "../helpers/data-loaders/initDataLoaders";

@ObjectType()
export class AuthenticationResponse {
  @Field()
  token!: string;

  @Field()
  user!: User;
}

export interface Context {
  req: Request;
  authorization?: string;
  user?: User;
  loaders: Loaders;
  cookies: Record<string, string>;
}
