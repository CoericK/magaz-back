import { FindManyOptions, FindOneOptions } from "typeorm";

// TODO: implement BaseDAL in all DALs

export interface BaseDAL<T> {
  findOneById(id: string): Promise<T | undefined>;

  findOne(options: FindOneOptions<T>): Promise<T | undefined>;

  find(options: FindManyOptions | null): Promise<T[]>;

  createOne(model: Partial<T>): Promise<T>;
}
