import { Arg, Ctx, Query, Resolver } from "type-graphql";

import { AuthenticationResponse, Context } from "../../../utils/types/gql-types";
import { LoginInputType } from "../shared/AuthInputTypes";
import { SignInService } from "./SignInService";

@Resolver()
export class SignInResolver {
  constructor(private readonly signInService: SignInService) {}

  @Query(returns => AuthenticationResponse)
  async signIn(
    @Arg("loginInput", type => LoginInputType) loginInput: LoginInputType,
    @Ctx() context: Context
  ): Promise<AuthenticationResponse> {
    return this.signInService.signIn(loginInput, context);
  }

  @Query(returns => Boolean, { nullable: true })
  async logout(@Ctx() context: Context): Promise<void> {
    return this.signInService.logout(context);
  }
}
