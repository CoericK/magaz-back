import { Service } from "typedi";

import { NoUserError } from "../../../utils/helpers/Errors";
import { comparePassword } from "../../../utils/helpers/bcryptHelper";
import {
  clearRefreshToken,
  setRefreshToken
} from "../../../utils/helpers/refreshToken";
import { generateAccessToken } from "../../../utils/helpers/accessToken";
import { LoginInputType } from "../shared/AuthInputTypes";
import {
  AuthenticationResponse,
  Context
} from "../../../utils/types/gql-types";
import { UserDAL } from "../../users/UserDAL";
import { RefreshTokenDAL } from "../../refresh-tokens/RefreshTokenDAL";

@Service()
export class SignInService {
  constructor(
    private readonly userDAL: UserDAL,
    private readonly refreshTokenDAL: RefreshTokenDAL
  ) {}

  async signIn(
    loginInput: LoginInputType,
    context: Context
  ): Promise<AuthenticationResponse> {
    const { password, email } = loginInput;

    const user = await this.userDAL.findOne({
      where: { email },
      relations: ["roles"]
    });

    if (!user) throw new NoUserError();

    const passwordCheck = await comparePassword(password, user.password);

    if (!passwordCheck) throw new NoUserError();

    const refreshToken = await this.refreshTokenDAL.createOne({
      user: user.id
    });

    setRefreshToken(refreshToken.id, context);

    return {
      user,
      token: await generateAccessToken(user.id)
    };
  }

  async logout(context: Context): Promise<void> {
    const { refresh_token: refreshToken } = context.cookies;
    await this.refreshTokenDAL.delete(refreshToken);
    clearRefreshToken(context);
  }
}
