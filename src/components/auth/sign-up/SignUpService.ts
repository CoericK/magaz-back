import { Service } from "typedi";
import {
  AuthenticationResponse,
  Context
} from "../../../utils/types/gql-types";
import { UserExistsError } from "../../../utils/helpers/Errors";
import { hashPassword } from "../../../utils/helpers/bcryptHelper";
import { generateAccessToken } from "../../../utils/helpers/accessToken";
import { setRefreshToken } from "../../../utils/helpers/refreshToken";
import { SignUpInputType } from "../shared/AuthInputTypes";
import { UserDAL } from "../../users/UserDAL";
import { RefreshTokenDAL } from "../../refresh-tokens/RefreshTokenDAL";
import { ConfirmCodeService } from "../shared/ConfirmCodeService";
import { OrderStatusEnum } from "../../../utils/enums/OrderStatus.enum";
import { User } from "../../../models/User";

@Service()
export class SignUpService {
  constructor(
    private readonly userDAL: UserDAL,
    private readonly refreshTokenDAL: RefreshTokenDAL,
    private readonly confirmCodeService: ConfirmCodeService
  ) {}

  async signUp(
    signUpInput: SignUpInputType,
    context: Context
  ): Promise<AuthenticationResponse> {
    const { password, email, phone } = signUpInput;

    const userExists = await this.userDAL.findOne({ where: { email, phone } });
    if (userExists) throw new UserExistsError();

    const hashedPassword = await hashPassword(password);

    const user = await this.createUserWithCart({
      ...signUpInput,
      password: hashedPassword
    });

    const [refreshToken, token] = await Promise.all([
      this.refreshTokenDAL.createOne({ user: user.id }),
      generateAccessToken(user.id)
    ]);
    setRefreshToken(refreshToken.id, context);

    this.confirmCodeService.generateAndSendConfirmCode(user);

    return {
      token,
      user
    };
  }

  async createUserWithCart(user: Partial<User>): Promise<User> {
    return this.userDAL.createOne({
      ...user,
      orders: [{ status: OrderStatusEnum.draft }]
    });
  }
}
