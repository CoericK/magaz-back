import { Service } from "typedi";
import { Arg, Authorized, Ctx, Mutation } from "type-graphql";

import { Context } from "../../../utils/types/gql-types";
import { UserConfirmService } from "./UserConfirmService";

@Service()
export class UserConfirmResolver {
  constructor(private readonly userConfirmService: UserConfirmService) {}

  @Mutation(returns => Boolean)
  async confirmUser(
    @Arg("code", type => String) code: string
  ): Promise<boolean> {
    return this.userConfirmService.confirmUser(code);
  }

  @Mutation(returns => Boolean)
  @Authorized()
  async resendConfirmCode(
    @Arg("userId", type => String) userId: string,
    @Ctx() context: Context
  ): Promise<boolean> {
    return this.userConfirmService.resendConfirmCode(userId, context);
  }
}
