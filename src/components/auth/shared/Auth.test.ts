import { Connection } from "typeorm";
import faker from "faker";
import Container from "typedi";
import { graphql, GraphQLSchema } from "graphql";

import { testConnection } from "../../../utils/test-utils/testConnection";
import { schemaLoader } from "../../../utils/loaders/schema";
import { UserDAL } from "../../users/UserDAL";
import { RefreshTokenDAL } from "../../refresh-tokens/RefreshTokenDAL";
import { Context } from "../../../utils/types/gql-types";
import { ConfirmUserMailerService } from "../../mailer/ConfirmUserMailerService";
import { CookieOptions } from "express";
import { RedisClient } from "../../redis/RedisClient";
import v4 from "uuid/v4";
import { Constants } from "../../../utils/enums/constants";

let conn: Connection;
let schema: GraphQLSchema;
let userDAL: UserDAL;
let refreshTokenDAL: RefreshTokenDAL;
let redisClient: RedisClient;

class MockConfirmSignUpMailService {
  send(): void {
    return;
  }
}

const contextMock = {
  req: {
    res: {
      cookie(name: string, val: string, options: CookieOptions): void {
        return;
      },
      clearCookie(name: string, options?: object): void {
        return;
      }
    }
  }
};

const registerMutation = `
  mutation registration($data: SignUpInputType!) {
  signUp(
    user: $data
  ) {
    token
    user {
      name
      dateOfBirth
      phone
      email
    }
  }
}
`;

const SignInQuery = `
  query signIn($data: LoginInputType!) {
    signIn(loginInput: $data) {
      token
      user {
        name
        dateOfBirth
        phone
        email
      }
    }
  }
`;

const refreshTokenQuery = `
  query refreshToken {
    refreshToken {
      token
      user {
        name
        dateOfBirth
        phone
        email
      }
    }
  }
`;

const logoutQuery = `
  query logout {
    logout
  }
`;

const confirmUserMutation = `
  mutation confirmUser($code: String!) {
    confirmUser(code: $code)
  }
`;

const user = {
  name: faker.name.firstName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  phone: faker.phone.phoneNumber("###########"),
  dateOfBirth: new Date().toISOString()
};

beforeAll(async () => {
  conn = await testConnection();
  schema = await schemaLoader();
  userDAL = Container.get(UserDAL);
  refreshTokenDAL = Container.get(RefreshTokenDAL);
  redisClient = Container.get(RedisClient);

  Container.set(ConfirmUserMailerService, new MockConfirmSignUpMailService());
});

afterAll(async done => {
  await conn.close();
  redisClient.disconnect();
  done();
});

describe("Auth", () => {
  it("Creates user", async () => {
    const response = await graphql({
      schema,
      source: registerMutation,
      contextValue: contextMock,
      variableValues: {
        data: user
      }
    });

    expect(response?.data?.signUp?.token).toBeDefined();
    expect(response).toMatchObject({
      data: {
        signUp: {
          user: {
            name: user.name,
            email: user.email,
            phone: user.phone,
            dateOfBirth: user.dateOfBirth
          }
        }
      }
    });

    const dbUser = await userDAL.findOne({ where: { email: user.email } });
    expect(dbUser).toBeDefined();
  });

  it("SignIn user", async () => {
    const loginInput = {
      email: user.email,
      password: user.password
    };

    const response = await graphql({
      schema,
      source: SignInQuery,
      contextValue: contextMock,
      variableValues: {
        data: loginInput
      }
    });

    expect(response?.data?.signIn?.token).toBeDefined();
    expect(response).toMatchObject({
      data: {
        signIn: {
          user: {
            name: user.name,
            email: user.email,
            phone: user.phone,
            dateOfBirth: user.dateOfBirth
          }
        }
      }
    });
  });

  it("refreshes user's token", async () => {
    const dbUser = await userDAL.findOne({ where: { email: user.email } });
    if (!dbUser?.id) return;

    const refreshToken = await refreshTokenDAL.createOne({ user: dbUser.id });

    const response = await graphql({
      schema,
      source: refreshTokenQuery,
      contextValue: {
        cookies: {
          refresh_token: refreshToken.id
        }
      } as Partial<Context>
    });

    expect(response?.data?.refreshToken?.token).toBeDefined();
  });

  it("deletes refresh token", async () => {
    const refreshToken = await refreshTokenDAL.findOneById();

    if (!refreshToken) return;

    await graphql({
      schema,
      source: logoutQuery,
      contextValue: {
        cookies: {
          refresh_token: refreshToken.id
        }
      } as Partial<Context>
    });

    const dbRefreshToken = await refreshTokenDAL.findOneById(refreshToken.id);

    expect(dbRefreshToken).not.toBeDefined();
  });

  it("Confirms users email", async () => {
    const dbUser = await userDAL.findOne({ where: { email: user.email } });

    const confirmCode = v4();
    if (!dbUser) throw new Error("User not found");

    redisClient.setExp(
      confirmCode,
      Constants.CONFIRM_LINK_EXPIRATION_TIME,
      dbUser.id
    );

    const result = await graphql({
      schema,
      source: confirmUserMutation,
      variableValues: {
        code: confirmCode
      }
    });

    expect(result?.data?.confirmUser).toBeTruthy();
  });

  //TODO: Test for resend confirm code
});
