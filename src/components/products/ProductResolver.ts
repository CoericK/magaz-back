import { Resolver, Mutation, Arg } from "type-graphql";
import { Product } from "../../models/Product";
import { CreateProductInputType } from "./ProductInputTypes";
import {ProductService} from "./ProductService";

@Resolver(of => Product)
export class ProductResolver {
  constructor(private readonly productService: ProductService) {}

  @Mutation(returns => Product)
  async createProduct(
    @Arg("productInput") productInput: CreateProductInputType
  ): Promise<Product> {
    return await this.productService.createProduct(productInput);
  }
}
