import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository, FindManyOptions, FindOneOptions } from "typeorm";

import { Product } from "../../models/Product";
import { BaseDAL } from "../../utils/types/BaseDAL";

@Service()
export class ProductDAL implements BaseDAL<Product> {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>
  ) {}

  async createOne(productInput: Partial<Product>): Promise<Product> {
    return await this.productRepository.save(productInput);
  }

  async findOneById(id: string): Promise<Product | undefined> {
    return this.productRepository.findOne(id);
  }

  async findOne(options: FindOneOptions): Promise<Product | undefined> {
    return this.productRepository.findOne(options);
  }

  async find(options: FindManyOptions): Promise<Product[]> {
    return await this.productRepository.find(options);
  }
}
