import { InputType, Field, ID } from "type-graphql";
import { Order } from "../../models/Order";
import { OrderSet } from "../../models/OrderSet";
import { OrderItem } from "../../models/OrderItem";
import { OrderStatusEnum } from "../../utils/enums/OrderStatus.enum";
import { Product } from "../../models/Product";

@InputType()
export class CreateOrderItemInputType {
  @Field()
  quantity!: number;

  @Field()
  product!: string;
}

@InputType()
export class CreateOrderSetInputType implements Partial<OrderSet> {
  @Field(() => [CreateOrderItemInputType])
  items!: OrderItem[];
}

@InputType()
export class CreateOrderInputType implements Partial<Order> {
  @Field(type => [CreateOrderSetInputType], { nullable: true })
  itemSets?: OrderSet[];

  @Field()
  status!: OrderStatusEnum;
}

@InputType()
export class ProductInputType implements Partial<Product> {
  @Field(type => ID)
  id!: string;
}
