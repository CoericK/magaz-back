import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { FindManyOptions, FindOneOptions, Repository } from "typeorm";
import { RefreshToken } from "../../models/RefreshToken";
import { BaseDAL } from "../../utils/types/BaseDAL";

@Service()
export class RefreshTokenDAL implements BaseDAL<RefreshToken> {
  constructor(
    @InjectRepository(RefreshToken)
    private readonly refreshTokenRepository: Repository<RefreshToken>
  ) {}

  async createOne(token: Partial<RefreshToken>): Promise<RefreshToken> {
    return await this.refreshTokenRepository.save({ user: token.user });
  }

  async delete(tokenID: string): Promise<void> {
    await this.refreshTokenRepository.delete(tokenID);
  }

  async find(options: FindManyOptions): Promise<RefreshToken[]> {
    return this.refreshTokenRepository.find(options);
  }

  async findOne(options: FindOneOptions): Promise<RefreshToken | undefined> {
    return this.refreshTokenRepository.findOne(options);
  }

  async findOneById(tokenID?: string): Promise<RefreshToken | undefined> {
    return await this.refreshTokenRepository.findOne(tokenID, {
      loadRelationIds: { relations: ["user"] }
    });
  }
}
