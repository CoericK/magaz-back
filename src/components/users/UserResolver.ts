/*
 * User resolver
 */

import { Resolver, FieldResolver, Root, Ctx } from "type-graphql";

import { User } from "../../models/User";
import { Context } from "../../utils/types/gql-types";
import { Order } from "../../models/Order";

@Resolver(of => User)
export class UserResolver {
  @FieldResolver()
  async cart(
    @Root() user: User,
    @Ctx() context: Context
  ): Promise<Order> {
    return await context.loaders.cartLoader.load(user.id);
  }
}
