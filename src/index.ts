/*
 * Bootstrap file
 */
import "reflect-metadata";
import { ApolloServer } from "apollo-server-express";

import app from "./utils/loaders/app";
import { env } from "./config/env";
import { initDataLoaders } from "./utils/helpers/data-loaders/initDataLoaders";
import { loadDb } from "./utils/loaders/db";
import { schemaLoader } from "./utils/loaders/schema";
import {Context} from "./utils/types/gql-types";

// * Bootstrap server
async function bootstrap(): Promise<void> {
  try {
    // * DB connection loader
    await loadDb();

    // * Building a schema
    const schema = await schemaLoader();

    // * Create apollo server
    const server = new ApolloServer({
      schema,
      context: ({ req }): Context => {
        return {
          req,
          cookies: req.cookies,
          authorization: req.headers && req.headers.authorization,
          loaders: initDataLoaders()
        };
      },
      introspection: env.apollo.introspection,
      playground: env.apollo.playground,
      tracing: env.apollo.tracing,
      debug: env.isProduction
    });

    // * Apply express to apollo server
    server.applyMiddleware({ app });

    await app.listen(env.port);

    console.log(`Server started on port: ${env.port}`);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

bootstrap();
