/*
 * Order entity
 */
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  Column,
  OneToMany,
  Index
} from "typeorm";
import { Field, ID, ObjectType } from "type-graphql";

import { User } from "./User";
import { OrderStatusEnum } from "../utils/enums/OrderStatus.enum";
import { OrderSet } from "./OrderSet";

@Entity()
@ObjectType()
export class Order {
  @Field(type => ID)
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @Field(() => User)
  @Index()
  @ManyToOne(
    type => User,
    user => user.orders
  )
  user!: User;

  @Field()
  @Column()
  status!: OrderStatusEnum;

  @Field()
  @CreateDateColumn()
  createdAt!: Date;

  @Field(() => [OrderSet])
  @OneToMany(
    type => OrderSet,
    orderSet => orderSet.order,
    { cascade: ["insert"], eager: true }
  )
  itemSets!: OrderSet[];
}
