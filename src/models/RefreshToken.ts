/*
 * Refresh token entity
 */

import { Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { User } from "./User";

@Entity()
export class RefreshToken {
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @ManyToOne(
    type => User,
    user => user.tokens,
    { onDelete: "CASCADE" }
  )
  user!: string;
}
