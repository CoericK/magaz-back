import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { ObjectType, Field } from "type-graphql";
import { OrderItem } from "./OrderItem";

@Entity()
@ObjectType()
export class Product {
  @Field()
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @Column()
  @Field()
  price!: number;

  @Column()
  @Field()
  name!: string;

  @Column()
  @Field()
  description!: string;

  @Column()
  @Field()
  inStock!: number;

  @OneToMany(
    type => OrderItem,
    orderItem => orderItem.product,
    { cascade: ["insert", "update"] }
  )
  orderItems?: OrderItem[];
}
