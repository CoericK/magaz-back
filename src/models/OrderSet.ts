/*
 * Order set entity
 */

import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { ObjectType, Field } from "type-graphql";
import { Order } from "./Order";
import { OrderItem } from "./OrderItem";

@Entity()
@ObjectType()
export class OrderSet {
  @PrimaryGeneratedColumn("uuid")
  readonly id!: string;

  @ManyToOne(
    type => Order,
    order => order.itemSets,
    { cascade: ["insert"] }
  )
  order!: string;

  @OneToMany(
    type => OrderItem,
    orderItem => orderItem.orderSet,
    { cascade: ["insert"] }
  )
  @Field(type => [OrderItem])
  items!: OrderItem[];
}
